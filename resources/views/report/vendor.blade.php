<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')

@section('styles')

@endsection

@section('content')

    <div class="box">

        <div class="box-header">
            <div class="row">
                <div class="col-md-8">
                    <div class="box-title">
                        Daftar Vendor
                    </div>
                </div>
                <div class="col-md-4 pull-right">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" class="form-control" name="filter" value=""/>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    <table id='table' class="table table-hover table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Vendor</th>
                            <th>Total Penjualan</th>
                            <th>Total Penghasilan</th>
                        </tr>
                        </thead>
                        <tbody id="data">
                        @foreach($result as $row)
                            <tr>
                                <td>{{$row['no']}}</td>
                                <td>{{$row['nama']}}</td>
                                <td class="text-center">{{$row['total']}}</td>
                                <td class="text-right">{{$row['total_price']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script>

        $(function () {
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
                sDate = mm + '/01/' + yyyy;
                eDate = mm + '/' + dd + '/' + yyyy;

                if ($('input[name="filter"]').val() == '' || $('input[name="filter"]').val() == null){
                    $('input[name="filter"]').val(sDate+' - '+eDate);
                }


            $('#table').DataTable({
                "order": [] //Initial no order.
            });

            $('input[name="filter"]').daterangepicker({
                opens: "right",
                ranges: {
                    'Hari Ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '1 Minggu Lalu': [moment().subtract(6, 'days'), moment()],
                    '1 Bulan Lalu': [moment().subtract(29, 'days'), moment()],
                    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')]
                },
                alwaysShowCalendars: true,
                "locale": {
                    "separator": " - ",
                    "applyLabel": "Apply",
                    "cancelLabel": "Cancel",
                    "customRangeLabel": "Custom",
                    "daysOfWeek": ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
                    "monthNames": ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                    "firstDay": 1
                }
            });

            $('input[name="filter"]').on('apply.daterangepicker', function (ev, picker) {
                ev.preventDefault();
                $.ajax({
                    url: "vendor-datatable",
                    type: "POST",
                    data: {
                        startDate: picker.startDate.format('YYYY-MM-DD'),
                        endDate: picker.endDate.format('YYYY-MM-DD')
                    },
                    success: function (result) {
                        // console.log(result);
                        if (result.length > 0) {
                            $('#table').DataTable().destroy();

                            $("#data").children().remove();
                            $.each(result.slice(0, result.length), function (i, data) {
                                $("#data").append("<tr>" +
                                    "<td>" + data.no + "</td>" +
                                    "<td>" + data.nama + "</td>" +
                                    "<td class='text-center'>" + data.total + "</td>" +
                                    "<td class='text-right'>" + data.total_price + "</td>" +
                                    "</tr>");
                            });

                            $('#table').DataTable({
                                "order": [] //Initial no order.
                            });
                        }
                    },
                    error: function (e) {
                        alert('Error occurred while request data');
                    }
                });
            });

        });
    </script>
@endsection