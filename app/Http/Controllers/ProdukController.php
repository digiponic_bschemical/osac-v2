<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProdukController extends Controller{
  
  public function getProduk($id){
    $barang = DB::table('tb_produk')->select('*')->where('id', $id)->first();
    return response()->json($barang);
  }

  public function getJasa($id){
    $barang = DB::table('tb_jasa')->select('*')->where('id', $id)->first();
    return response()->json($barang);
  }

}
