<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;

class AdminLaporanVendorJasa extends \crocodicstudio\crudbooster\controllers\CBController
{
    public function cbInit()
    {

        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->title_field = "nama_jasa";
        $this->limit = "20";
        $this->orderby = "id,desc";
        $this->global_privilege = false;
        $this->button_table_action = true;
        $this->button_bulk_action = true;
        $this->button_action_style = "button_icon";
        $this->button_add = false;
        $this->button_edit = true;
        $this->button_delete = true;
        $this->button_detail = true;
        $this->button_show = false;
        $this->button_filter = true;
        $this->button_import = false;
        $this->button_export = false;
        $this->table = "tb_penjualan_jasa_detail";
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = ["label" => "Penjualan Jasa", "name" => "id_penjualan_jasa", "join" => "tb_penjualan_jasa,id"];
        $this->col[] = ["label" => "Kode Penjualan Jasa", "name" => "kode_penjualan_jasa"];
        $this->col[] = ["label" => "Jasa", "name" => "id_jasa", "join" => "tb_jasa,id"];
        $this->col[] = ["label" => "Nama Jasa", "name" => "nama_jasa"];
        $this->col[] = ["label" => "Harga", "name" => "harga"];
        $this->col[] = ["label" => "Durasi", "name" => "durasi"];
        $this->col[] = ["label" => "Diskon Tipe", "name" => "diskon_tipe"];
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ['label' => 'Penjualan Jasa', 'name' => 'id_penjualan_jasa', 'type' => 'select2', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10', 'datatable' => 'penjualan_jasa,id'];
        $this->form[] = ['label' => 'Kode Penjualan Jasa', 'name' => 'kode_penjualan_jasa', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Jasa', 'name' => 'id_jasa', 'type' => 'select2', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10', 'datatable' => 'jasa,id'];
        $this->form[] = ['label' => 'Nama Jasa', 'name' => 'nama_jasa', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Harga', 'name' => 'harga', 'type' => 'money', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Durasi', 'name' => 'durasi', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Diskon Tipe', 'name' => 'diskon_tipe', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Diskon Nominal', 'name' => 'diskon_nominal', 'type' => 'money', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Subtotal', 'name' => 'subtotal', 'type' => 'money', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Total', 'name' => 'total', 'type' => 'money', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
        # END FORM DO NOT REMOVE THIS LINE

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)

    public function getIndex()
    {
        //First, Add an auth
        if (!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        //Create your own query
        $data = [];
        $data['page_title'] = 'Penjualan Jasa per Vendor';
        $result = DB::table('tb_jasa')
            ->join('tb_vendor as v', 'v.id', '=', 'tb_jasa.id_vendor')
            ->leftJoin('tb_penjualan_jasa_detail', 'tb_penjualan_jasa_detail.id_jasa', '=', 'tb_jasa.id')
            ->select(array('id_jenis_jasa', 'v.nama', DB::raw('COUNT(tb_penjualan_jasa_detail.id_jasa) as total'), DB::raw('SUM(tb_penjualan_jasa_detail.harga) as total_price')))
            ->whereIn('tb_penjualan_jasa_detail.id', function($query){
                $query->select('tb_penjualan_jasa_detail.id')
                    ->from('tb_penjualan_jasa_detail')
                    ->join('tb_penjualan_jasa', 'tb_penjualan_jasa_detail.id_penjualan_jasa', '=', 'tb_penjualan_jasa.id')
                    ->where('tb_penjualan_jasa.deleted_at', null)
                    ->where('tb_penjualan_jasa.status_pembayaran', 26)
                    ->whereBetween('tb_penjualan_jasa.created_at', [date("Y-m").'-01 00:00:00', date("Y-m-d").' 23:59:59']);
            })
            ->groupBy('id_vendor')
            ->orderby('id_vendor', 'asc')->get();

        $records = [];
        $total = 0;
        $total_price = 0;
        $no = 1;
        foreach ($result as $r){
            $temp = [];
            $total += $r->total;
            $total_price += $r->total_price;

            $temp['no'] = $no;
            $temp['nama'] = $r->nama;
            $temp['total'] = $r->total;
            $temp['total_price'] = number_format($r->total_price, 0, ".", ".");
            $records[] = $temp;
            $no++;
        }

        $records[] = ['no' => '', 'nama' => 'TOTALS', 'total' => $total, 'total_price' => number_format($total_price, 0, ".", ".")];
        $data['result'] = $records;

        //Create a view. Please use `cbView` method instead of view method from laravel.
        $this->cbView('report/vendor', $data);
    }

    public function getData()
    {
        //Create your own query
        $result = DB::table('tb_jasa')
            ->join('tb_vendor as v', 'tb_jasa.id_vendor', '=', 'v.id')
            ->leftJoin('tb_penjualan_jasa_detail', 'tb_penjualan_jasa_detail.id_jasa', '=', 'tb_jasa.id')
            ->select(array('id_jenis_jasa', 'v.nama', DB::raw('COUNT(tb_penjualan_jasa_detail.id_jasa) as total'), DB::raw('SUM(tb_penjualan_jasa_detail.harga) as total_price')))
            ->whereIn('tb_penjualan_jasa_detail.id', function($query){
                $query->select('tb_penjualan_jasa_detail.id')
                    ->from('tb_penjualan_jasa_detail')
                    ->join('tb_penjualan_jasa', 'tb_penjualan_jasa_detail.id_penjualan_jasa', '=', 'tb_penjualan_jasa.id')
                    ->where('tb_penjualan_jasa.deleted_at', null)
                    ->where('tb_penjualan_jasa.status_pembayaran', 26)
                    ->whereBetween('tb_penjualan_jasa.tanggal', [$_POST['startDate'].' 00:00:00', $_POST['endDate'].' 23:59:59']);
            })
            ->groupBy('id_vendor')
            ->orderby('id_vendor', 'asc')->get();

        $data = [];
        $total = 0;
        $total_price = 0;
        $no = 1;
        foreach ($result as $r){
            $temp = [];
            $total += $r->total;
            $total_price += $r->total_price;

            $temp['no'] = $no;
            $temp['nama'] = $r->nama;
            $temp['total'] = $r->total;
            $temp['total_price'] = number_format($r->total_price, 0, ".", ".");
            $data[] = $temp;
            $no++;
        }

        $data[] = ['no' => '', 'nama' => 'TOTALS', 'total' => $total, 'total_price' => number_format($total_price, 0, ".", ".")];

        //output to json format
        return response()->json($data, 200);
    }
}