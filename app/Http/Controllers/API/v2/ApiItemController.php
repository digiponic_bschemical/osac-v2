<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiItemController extends Controller{
    public $response = array('error' => false, 'msg' => null, 'data' => null);
    public $successStatus = 200;
    public function all($tipe){
      $data = DB::table('tb_item')
              ->leftJoin(DB::raw('tb_general as merek'), 'merek.id', '=', 'tb_item.id_merek')
              ->join(DB::raw('tb_general as tipe'), 'tipe.id', '=', 'tb_item.id_tipe')
              ->leftJoin('tb_vendor', 'tb_vendor.id', '=', 'tb_item.id_vendor')
              ->join(DB::raw('tb_general as kategori'), 'kategori.id', '=', 'tb_item.id_kategori')
              ->leftJoin(DB::raw('tb_general as satuan'), 'satuan.id', '=', 'tb_item.id_satuan')
              ->select('tb_item.id', 'tb_item.kode', 'tb_item.keterangan', DB::raw('merek.keterangan AS merek, 
              tb_vendor.nama AS nama_vendor, kategori.keterangan AS kategori, satuan.keterangan AS satuan'),
              'tb_item.harga_beli', 'tb_item.harga_jual', 'tb_item.durasi', 'tb_item.qty', 'tb_item.jenis_barcode', 
              'tb_item.deskripsi', 'tb_item.gambar')->where('tb_item.deleted_at', NULL)
              ->where('tipe.keterangan', $tipe)->get();
      return response()->json(['error' => false, 'msg' => 'Daftar Produk', 'data' => $data], $this->successStatus);
    }
}
