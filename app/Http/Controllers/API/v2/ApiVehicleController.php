<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Support\Facades\DB;

class ApiVehicleController extends Controller
{    
    public $response = array(
        'error' => false,
        'msg'   => null,
        'data'  => null
    );

    public function brands()
    {
        $brands = DB::table('tb_merek_kendaraan')
                    ->select('id','kode','keterangan')
                    ->get();

        foreach ($brands as $value) {
            $value->model = DB::table('tb_kendaraan')
                                ->select('id','kode','keterangan')
                                ->where('id_merek_kendaraan', $value->id)
                                ->get();            
        }
        
        $this->response['msg'] = 'List all brand';
        $this->response['data'] = $brands;

        return response()->json($this->response, 200);
        
    }

    public function type(Request $request)
    {
        /* {
            id_merek_kendaraan: int
        } */

        $type = DB::table('tb_kendaraan')
                        ->select('id','kode','keterangan','gambar','created_at')
                        ->where('id_merek_kendaraan',$request->id_merek_kendaraan)
                        ->get();

        foreach ($type as $value) {
            if($value->gambar == null)
                $value->gambar = url('/').'/logo.png';
            else
                $value->gambar = url('/').'/'.$value->gambar;
        }
        
        $this->response['msg'] = 'List vehicle by brand';
        $this->response['data'] = $type;

        return response()->json($this->response, 200);
        
    }

    public function listVehicleByUser(Request $request)
    {
        /* {
            email: string
        } */        
        $user = DB::table('tb_pelanggan')->where('email',$request->email)->first();
        $vehicle = DB::table('tb_pelanggan_kendaraan as pk')
                        ->join('tb_merek_kendaraan as mk','mk.id','=','pk.id_merek_kendaraan')
                        ->join('tb_kendaraan as k','k.id','=','pk.id_kendaraan')
                        ->select('pk.id','mk.keterangan as merek','k.keterangan as kendaraan','k.gambar','pk.nomor_polisi','pk.tahun','pk.warna','pk.primary','pk.created_at')
                        ->where('pk.id_pelanggan',$user->id)
                        ->whereNull('pk.deleted_at')
                        ->get();

        foreach ($vehicle as $value) {
            if($value->gambar == null)
                $value->gambar = url('/').'/logo.png';
            else
                $value->gambar = url('/').'/'.$value->gambar;
        }
                        
        $this->response['msg'] = 'List vehicle by user';
        $this->response['data'] = $vehicle;

        return response()->json($this->response, 200);
        
    }

    public function singleVehicleByUser(Request $request)
    {
        /* {
            id: int
        } */
        $vehicle = CRUDBooster::first('tb_pelanggan_kendaraan', $request->id);
        $this->response = $vehicle;
        $this->response->msg = 'Information single vehicle by user';

        return response()->json($this->response, 200);
        
    }

    public function saveVehicleByUser(Request $request)
    {
        /* {
            email: string,
            id_brands: int,
            id_type: int,
            police_number: string,
            year: int,
            color: string
        } */        

        $user = DB::table('tb_pelanggan')->where('email',$request->email)->first();                
        $data = array(
            'id_pelanggan'          => $user->id,
            'id_merek_kendaraan'    => $request->id_brands,
            'id_kendaraan'          => $request->id_type,
            'nomor_polisi'          => $request->police_number,
            'tahun'                 => $request->year,
            'warna'                 => $request->color,
            'created_at'            => date('Y-m-d H:i:s')
        );
        $vehicle = DB::table('tb_pelanggan_kendaraan')->insert($data);   
        if($vehicle){
            $this->response['msg'] = 'Save vehicle by user success';
        }else{
            $this->response['error'] = true;
            $this->response['msg'] = 'Save vehicle by user failed';
        }

        return response()->json($this->response, 200);
        
    }

    public function deleteVehicleByUser(Request $request)
    {
        /* {
            id: int
        } */
        $vehicle = DB::table('tb_pelanggan_kendaraan')->where('id', $request->id)->update(['deleted_at'=>date('Y-m-d H:i:s')]);
        if($vehicle){
            $this->response['msg'] = 'Delete vehicle by user success';
        }else{
            $this->response['error'] = true;
            $this->response['msg'] = 'Delete vehicle by user failed';
        }
        
        return response()->json($this->response, 200);
        
    }

    public function primaryVehicleByUser(Request $request)
    {
        /* {
            email: string,
            id: int
        } */        
        $user = DB::table('tb_pelanggan')->where('email', $request->email)->first();                 
        $listVehicle = DB::table('tb_pelanggan_kendaraan')->select('id')->where('id_pelanggan',$user->id)->get();
        
        foreach ($listVehicle as $value) { 
            $listId[] = $value->id; 
        }        

        $setNonPrimary = DB::table('tb_pelanggan_kendaraan')->whereIn('id',$listId)->update(['primary'=>0]);
        $setPrimary = DB::table('tb_pelanggan_kendaraan')->where('id', $request->id)->update(['primary'=>1]);
        
        if($setPrimary){
            $this->response['msg'] = 'Set as primary vehicle by user success';
        }else{
            $this->response['error'] = true;
            $this->response['msg'] = 'Set as primary vehicle by user failed';
        }
        
        return response()->json($this->response, 200);
        
    }
}
