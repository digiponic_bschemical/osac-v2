<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Support\Facades\DB;

class ApiServicesController extends Controller
{
    public $table = 'tb_jasa';
    public $response = array(
        'error' => true,
        'msg'   => null,
        'data'  => null
    );

    public function list(Request $request)
    {
        /*
            {
                "id_jenis_jasa": int
            } 
        */
        $input = $request->all();
        $query = DB::table($this->table);
        if(empty($input)){
            $list = $query->select('id','id_jenis_jasa','id_vendor','kode','keterangan','gambar','deskripsi')->get();
            foreach ($list as $value) {
                if($value->gambar == null)
                    $value->gambar = url('/').'/logo.png';
                else
                    $value->gambar = url('/').'/'.$value->gambar;
            }

            $this->response['error'] = false;
            $this->response['msg'] = 'All list services';
        }else{
            $list = $query->select('id','kode','keterangan','gambar','deskripsi')
                            ->where('id_jenis_jasa', $request->id_jenis_jasa)
                            ->get();

            foreach ($list as $value) {
                if($value->gambar == null)
                    $value->gambar = url('/').'/logo.png';
                else
                    $value->gambar = url('/').'/'.$value->gambar;
            }

            $this->response['error'] = false;
            $this->response['msg'] = 'List service by categori';
            
        }        

        $this->response['data'] = $list;
        return response()->json($this->response, 200);
        
    }

    public function single(Request $request)
    {
        /*
            {
                "id": int
            } 
        */
        $single = DB::table($this->table)
                        ->select('id','kode','keterangan','gambar','deskripsi')
                        ->where('id', $request->id)
                        ->first();
        
        if(empty($single)){
            $this->response['error'] = false;
            $this->response['msg'] = 'Service not found';
        }else{    
            $this->response = $single;
            $this->response->error = false;
            $this->response->msg = 'Information single services';
        }

        return response()->json($this->response, 200); 
    }

    public function price(Request $request)
    {
        /* {
            id_kendaraan: int
        } */

        $model = CRUDBooster::first('tb_kendaraan', $request->id_kendaraan);
        $service = DB::table('tb_jasa as j')                        
                        ->select('id','id_jenis_jasa','kode','keterangan','gambar','deskripsi')
                        ->get();
        foreach ($service as $value) {
            $value->harga = DB::table('tb_harga_jasa')->where('id_jasa', $value->id)->where('id_jenis_kendaraan', $model->id_jenis_kendaraan)->value('harga');
            $value->durasi = DB::table('tb_durasi_jasa')->where('id_jasa', $value->id)->where('id_jenis_kendaraan', $model->id_jenis_kendaraan)->value('durasi');
            if($value->gambar == null)
                $value->gambar = url('/').'/logo.png';
            else
                $value->gambar = url('/').'/'.$value->gambar;
        }

        $this->response['error'] = false;
        $this->response['msg'] = 'Price services by vehicle';
        $this->response['data'] = $service;
        return response()->json($this->response, 200); 
    }
    
}
