<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ApiNewsPromoController extends Controller
{
    public $table = 'tb_berita_promosi';
    public $response = array(
        'error' => false,
        'msg'   => null,
        'data'  => null
    );

    public function list(Request $request)
    {
        $query = DB::table($this->table)
                    ->select('id','kode','keterangan','gambar','deskripsi','created_at')
                    ->whereNull('deleted_at');
                    
        if(empty($request->id)){
            $list = $query->get();
        }else{
            $list = $query->where('id', $request->id)->first();
            $list->created_at = date('d F Y H:i',strtotime($list->created_at));
            if($list->gambar == null)
                $list->gambar = url('/').'/logo.png';
            else
                $list->gambar = url('/').'/'.$list->gambar;

            $this->response = $list;
            $this->response->error = false;
            $this->response->msg = 'Single news and promo';
            return response()->json($this->response, 200);
        }

        if(!$list){
            $this->response['error'] = true;
            $this->response['msg'] = 'Error get list news and promo';
            return response()->json($this->response, 401);
        }

        if(empty($list)){
            $this->response['msg'] = 'Empty list news and promo';
        }else{
            foreach ($list as $value) {
                $value->created_at = date('d F Y H:i',strtotime($value->created_at));
                if($value->gambar == null)
                    $value->gambar = url('/').'/logo.png';
                else
                    $value->gambar = url('/').'/'.$value->gambar;
            }
    
            $this->response['msg'] = 'List news and promo';
            $this->response['data'] = $list;
        }
        
        return response()->json($this->response, 200);    
    }

}
